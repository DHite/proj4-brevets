# Project 4: Brevet time calculator with Ajax

Reimplement the RUSA ACP controle time calculator with flask and ajax.

Credits to Michal Young for the initial version of this code.

## ACP controle times

That's "controle" with an 'e', because it's French, although "control" is also accepted. Controls are points where a rider must obtain proof of passage, and control[e] times are the minimum and maximum times by which the rider must arrive at the location.   

The algorithm for calculating controle times is described here (https://rusa.org/pages/acp-brevet-control-times-calculator). Additional background information is given here (https://rusa.org/pages/rulesForRiders).

This project is  essentially replacing the calculator here (https://rusa.org/octime_acp.html).

## AJAX and Flask reimplementation

The RUSA controle time calculator is a Perl script that takes an HTML form and emits a text page in the above link. 

* Each time a distance is filled in, the corresponding open and close times will be filled in with Ajax.   

* The logic for the calculations is in acp_times.py

* webpage is in ./brevets/templates/calc.html, and flask implementaion is in ./brevets/flask_brevets.py

## Testing

A test suite is included in ./brevets/test_acp_times.py

