"""
Test suite for acp_times.py

"""

import acp_times
import arrow

def test_narrow():
    start_time = arrow.get('2021-03-07T06:00:00')
    openz = '2021-03-07T06:00:00+00:00'
    open10 = '2021-03-07T06:18:00+00:00'
    open11 = '2021-03-07T06:19:00+00:00'
    open12 = '2021-03-07T06:21:00+00:00'
    open13 = '2021-03-07T06:23:00+00:00'
    open14 = '2021-03-07T06:25:00+00:00'
    closez = '2021-03-07T07:00:00+00:00'
    close10 = '2021-03-07T07:30:00+00:00'
    close11 = '2021-03-07T07:33:00+00:00'
    close12 = '2021-03-07T07:36:00+00:00'
    close13 = '2021-03-07T07:39:00+00:00'
    close14 = '2021-03-07T07:42:00+00:00'
    assert(openz == acp_times.open_time(0, 200, start_time.isoformat()))
    assert(open10 == acp_times.open_time(10, 200, start_time.isoformat()))
    assert(open11 == acp_times.open_time(11, 200, start_time.isoformat()))
    assert(open12 == acp_times.open_time(12, 200, start_time.isoformat()))
    assert(open13 == acp_times.open_time(13, 200, start_time.isoformat()))
    assert(open14 == acp_times.open_time(14, 200, start_time.isoformat()))
    assert(closez == acp_times.close_time(0, 200, start_time.isoformat()))
    assert(close10 == acp_times.close_time(10, 200, start_time.isoformat()))
    assert(close11 == acp_times.close_time(11, 200, start_time.isoformat()))
    assert(close12 == acp_times.close_time(12, 200, start_time.isoformat()))
    assert(close13 == acp_times.close_time(13, 200, start_time.isoformat()))
    assert(close14 == acp_times.close_time(14, 200, start_time.isoformat()))

def test_day_change():
    start_time = arrow.get('2021-04-07T22:00:00')
    openz = '2021-04-07T22:00:00+00:00'
    open60 = '2021-04-07T23:46:00+00:00'
    open100 = '2021-04-08T00:56:00+00:00'
    open200 = '2021-04-08T03:53:00+00:00'
    open250 = '2021-04-08T05:27:00+00:00'
    open350 = '2021-04-08T08:34:00+00:00'
    open400 = '2021-04-08T10:08:00+00:00'

    closez = '2021-04-07T23:00:00+00:00'
    close60 = '2021-04-08T02:00:00+00:00'
    close100 = '2021-04-08T04:40:00+00:00'
    close200 = '2021-04-08T11:20:00+00:00'
    close250 = '2021-04-08T14:40:00+00:00'
    close350 = '2021-04-08T21:20:00+00:00'
    close400 = '2021-04-09T01:00:00+00:00'

    assert(openz == acp_times.open_time(0, 400, start_time.isoformat()))
    assert(open60 == acp_times.open_time(60, 400, start_time.isoformat()))
    assert(open100 == acp_times.open_time(100, 400, start_time.isoformat()))
    assert(open200 == acp_times.open_time(200, 400, start_time.isoformat()))
    assert(open250 == acp_times.open_time(250, 400, start_time.isoformat()))
    assert(open350 == acp_times.open_time(350, 400, start_time.isoformat()))
    assert(open400 == acp_times.open_time(400, 400, start_time.isoformat()))

    assert(closez == acp_times.close_time(0, 400, start_time.isoformat()))
    assert(close60 == acp_times.close_time(60, 400, start_time.isoformat()))
    assert(close100 == acp_times.close_time(100, 400, start_time.isoformat()))
    assert(close200 == acp_times.close_time(200, 400, start_time.isoformat()))
    assert(close250 == acp_times.close_time(250, 400, start_time.isoformat()))
    assert(close350 == acp_times.close_time(350, 400, start_time.isoformat()))
    assert(close400 == acp_times.close_time(400, 400, start_time.isoformat()))

def test_month_change():
    start_time = arrow.get('2021-03-31T20:00:00')
    openz = '2021-03-31T20:00:00+00:00'
    open500 = '2021-04-01T11:28:00+00:00'
    open1000 = '2021-04-02T05:05:00+00:00'

    closez = '2021-03-31T21:00:00+00:00'
    close500 = '2021-04-02T05:20:00+00:00'
    close1000 = '2021-04-03T23:00:00+00:00'

    assert(openz == acp_times.open_time(0, 1000, start_time.isoformat()))
    assert(open500 == acp_times.open_time(500, 1000, start_time.isoformat()))
    assert(open1000 == acp_times.open_time(1000, 1000, start_time.isoformat()))

    assert(closez == acp_times.close_time(0, 1000, start_time.isoformat()))
    assert(close500 == acp_times.close_time(500, 1000, start_time.isoformat()))
    assert(close1000 == acp_times.close_time(1000, 1000, start_time.isoformat()))

def test_past_brevet():
    start_time = arrow.get('2020-12-25T06:00:00')
    openz = '2020-12-25T06:00:00+00:00'
    oopen = '2020-12-25T11:53:00+00:00'

    closez = '2020-12-25T07:00:00+00:00'
    oclose = '2020-12-25T19:30:00+00:00'

    assert(openz == acp_times.open_time(0, 200, start_time.isoformat()))
    assert(oopen == acp_times.open_time(200, 200, start_time.isoformat()))
    assert(oopen == acp_times.open_time(220, 200, start_time.isoformat()))
    assert(oopen == acp_times.open_time(500, 200, start_time.isoformat()))
    assert(closez == acp_times.close_time(0, 200, start_time.isoformat()))
    assert(oclose == acp_times.close_time(200, 200, start_time.isoformat()))
    assert(oclose == acp_times.close_time(220, 200, start_time.isoformat()))
    assert(oclose == acp_times.close_time(500, 200, start_time.isoformat()))

def test_year_change():
    start_time = arrow.get('2020-12-31T20:00:00')
    openz = '2020-12-31T20:00:00+00:00'
    open500 = '2021-01-01T11:28:00+00:00'
    open1000 = '2021-01-02T05:05:00+00:00'

    closez = '2020-12-31T21:00:00+00:00'
    close500 = '2021-01-02T05:20:00+00:00'
    close1000 = '2021-01-03T23:00:00+00:00'

    assert(openz == acp_times.open_time(0, 1000, start_time.isoformat()))
    assert(open500 == acp_times.open_time(500, 1000, start_time.isoformat()))
    assert(open1000 == acp_times.open_time(1000, 1000, start_time.isoformat()))

    assert(closez == acp_times.close_time(0, 1000, start_time.isoformat()))
    assert(close500 == acp_times.close_time(500, 1000, start_time.isoformat()))
    assert(close1000 == acp_times.close_time(1000, 1000, start_time.isoformat()))








